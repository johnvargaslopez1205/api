//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,Accept");
  next();
})

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/jvargas/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function (req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req, res){
  res.send("hola mundo");
});

app.put('/', function(req,res){
  res.send("put version cambiada");
});

app.delete('/', function(req,res){
  res.send("Detele");
});

app.get('/movimientos',function(req,res){
  clienteMLab.get('', function(err, resM, body){
    if(err) {
      console.log(body);
    }else {
      res.send(body);
    }
  });

});

app.post('/movimientos', function(req,res){
  clienteMLab.post('', req.body, function(err, resM, body){
      res.send(body);
  })
})


/**app.get('/clientes/:idcliente/:nombre', function(req,res){
  res.send("Aqui tiene al cliente: "+req.params.idcliente + "y su nombre: "+req.params.nombre););
});**/

app.get('/clientes/:idcliente', function(req,res){
  res.send("Aqui tiene al cliente: "+req.params.idcliente);
});


var movimientosJSON = require('./movimientosv1.json')
app.get('/v1/movimientos', function(req,res){
  res.send(movimientosJSON);
//  res.send('movimientosv1.json');
});
